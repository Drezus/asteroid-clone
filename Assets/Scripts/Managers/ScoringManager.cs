﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public enum ScoreType
{
    DestroyAsteroid,
    DestroyBigUFO,
    DestroySmallUFO,
    PerfectClear
}

public class ScoringManager : Singleton<ScoringManager>
{
    [Header("Score Configuration")]
    public int maxScoresToSave = 10;

    private const string saveDataPath = "/highScores.json";

    [HideInInspector]
    public ScoreList highScores;
    [HideInInspector]
    public Score thisGameScore = null;

    public readonly Dictionary<ScoreType, int> scoreValues = new Dictionary<ScoreType, int>()
    {
        { ScoreType.DestroyAsteroid, 300 },
        { ScoreType.DestroyBigUFO, 400 },
        { ScoreType.DestroySmallUFO, 500 },
        { ScoreType.PerfectClear, 1000 }
    };

    public void ApplyScoreSession()
    {
        if (highScores == null)
            LoadHighscores();

        thisGameScore.ship = GameManager.Instance.selectedShip;
        thisGameScore.stage = GameManager.Instance.currentStage;

        highScores.scoreList.Add(thisGameScore);

        ///Organizes the list before saving or making trim operations by using LINQ
        highScores.scoreList = highScores.scoreList.OrderByDescending(x => x.points).ToList();

        SaveJson(highScores);

        thisGameScore = new Score();
        thisGameScore.points = 0;
    }

    public void LoadHighscores()
    {
        if(highScores != null)
            highScores = LoadJson();
    }

    public void EarnPoints(ScoreType type)
    {
        thisGameScore.points += scoreValues[type];

        HUDManager.Instance.UpdateScoreText(thisGameScore.points);  
    }

    public void EarnPoints(int score)
    {
        thisGameScore.points += score;

        HUDManager.Instance.UpdateScoreText(thisGameScore.points);
    }

    public void GameOverSaveScore()
    {
        thisGameScore.stage = GameManager.Instance.currentStage;
        thisGameScore.ship = GameManager.Instance.selectedShip;

        highScores.scoreList.Add(thisGameScore);

        SaveJson(highScores);
    }

    #region Json saving and loading
    private void SaveJson(ScoreList score)
    {
        ///Trims the Highscore list to have only up to a certain amount of records.
        if (score.scoreList.Count > maxScoresToSave)
        {
            ScoreList trimmedList = new ScoreList();
            trimmedList.scoreList = new List<Score>();

            for (int i = 0; i < maxScoresToSave; i++)
            {
                trimmedList.scoreList.Add(score.scoreList[i]);
            }

            score = trimmedList;
        }

        string dataAsJson = JsonUtility.ToJson(score, true);

        string filePath = Application.persistentDataPath + saveDataPath;
        File.WriteAllText(filePath, dataAsJson);
    }

    private ScoreList LoadJson()
    {
        string filePath = Application.persistentDataPath + saveDataPath;

        ScoreList loadedList = new ScoreList();

        if (File.Exists(filePath))
        {
            ///Retrieve from saved json
            string dataAsJson = File.ReadAllText(filePath);
            JsonUtility.FromJsonOverwrite(dataAsJson, loadedList);
        }
        else
        {
            ///Fill with dummy data
            loadedList.scoreList = new List<Score>();

            loadedList.scoreList.Add(new Score() { points = 500, stage = 1, ship = 0 });
            loadedList.scoreList.Add(new Score() { points = 400, stage = 1, ship = 1 });
            loadedList.scoreList.Add(new Score() { points = 300, stage = 1, ship = 2 });
            loadedList.scoreList.Add(new Score() { points = 200, stage = 1, ship = 3 });

            SaveJson(loadedList);
        }

        return loadedList;
    }
    #endregion
}