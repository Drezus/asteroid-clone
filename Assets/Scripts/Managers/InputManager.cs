﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : Singleton<InputManager>
{
    public enum InputMethod
    {
        Keyboard,
        Touch
    }

    public InputMethod inputMethod;
    public GameObject mobileInputCanvas;

    [HideInInspector]
    public bool CanControl = true;
    [HideInInspector]
    public bool CanPause = false;

    public void ShowMobileInput(bool show)
    {
        ///Prevents showing when the Touch input is not explicitly enabled.
        if (inputMethod != InputMethod.Touch) return;

        mobileInputCanvas.SetActive(show);
    }

    #region Public Outputs
    private bool leftPressed = false;
    private bool rightPressed = false;
    private bool thrustPressed = false;
    private bool firePressed = false;
    private bool pausePressed = false;

    public bool LeftPressed { get { return leftPressed; } }
    public bool RightPressed { get { return rightPressed; } }
    public bool ThrustPressed { get { return thrustPressed; } }
    public bool FirePressed { get { return firePressed; } }
    public bool PausePressed { get { return pausePressed; } }
    #endregion

    #region Keyboard Inputs
    KeyCode leftKey = KeyCode.LeftArrow;
    KeyCode rightKey = KeyCode.RightArrow;
    KeyCode thrustKey = KeyCode.UpArrow;
    KeyCode fireKey = KeyCode.Space;
    KeyCode pauseKey = KeyCode.Escape;
    #endregion

    #region Touch Inputs
    bool leftTouch;
    bool rightTouch;
    bool thrustTouch;
    bool fireTouch;
    bool pauseTouch;

    public void LeftTouchPressed(bool pressed)
    {
        leftTouch = pressed;
    }

    public void RightTouchPressed(bool pressed)
    {
        rightTouch = pressed;
    }

    public void ThrustTouchPressed(bool pressed)
    {
        thrustTouch = pressed;
    }

    public void FireTouchPressed(bool pressed)
    {
        fireTouch = pressed;
    }

    public void PauseTouchPressed(bool pressed)
    {
        pauseTouch = pressed;
    }
    #endregion

    private void Update()
    {
        if(CanControl)
        {
            switch (inputMethod)
            {
                case InputMethod.Touch:
                    ListenTouch();
                    break;
                case InputMethod.Keyboard:
                default:
                    ListenKeyboard();
                    break;
            }
        }
        else
        {
            leftPressed = false;
            rightPressed = false;
            thrustPressed = false;
            firePressed = false;
            pausePressed = false;
        }
    }

    private void ListenKeyboard()
    {
        leftPressed = Input.GetKey(leftKey) ? true : false;
        rightPressed = Input.GetKey(rightKey) ? true : false;
        thrustPressed = Input.GetKey(thrustKey) ? true : false;
        firePressed = Input.GetKey(fireKey) ? true : false;
        pausePressed = Input.GetKey(pauseKey) ? true : false;
    }

    private void ListenTouch()
    {
        leftPressed = leftTouch ? true : false;
        rightPressed = rightTouch ? true : false;
        thrustPressed = thrustTouch ? true : false;
        firePressed = fireTouch ? true : false;
        pausePressed = pauseTouch ? true : false;
    }

    public void ResetTouches()
    {
        LeftTouchPressed(false);
        RightTouchPressed(false);
        ThrustTouchPressed(false);
        FireTouchPressed(false);
        PauseTouchPressed(false);
    }
}
