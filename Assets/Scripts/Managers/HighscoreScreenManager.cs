﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreScreenManager : MonoBehaviour
{
    [Header ("References")]
    public GameObject mainMenuCanvas;
    public GameObject retryButton;
    public ScrollRect scroll;

    [Header("Prefabs")]
    public GameObject scoreRowPrefab;
    public GameObject scrollContent;

    List<Score> list;
    List<GameObject> rowGOs;

	public void OnEnable ()
    {
        scroll.verticalNormalizedPosition = 1f;

        retryButton.SetActive(GameManager.Instance.currentStage > 0);

        list = ScoringManager.Instance.highScores.scoreList;
        rowGOs = new List<GameObject>();

        for (int i = 0; i < list.Count; i++)
        {
            var GO = Instantiate(scoreRowPrefab, scrollContent.transform);

            ScoreRowHUD row = GO.GetComponent<ScoreRowHUD>();

            row.SetRow(list[i]);
            rowGOs.Add(GO);
        }
    }

    public void OnDisable()
    {
        foreach(GameObject go in rowGOs)
        {
            Destroy(go);
        }
    }

    public void BTN_MainMenu()
    {
        FadeAnimManager.Instance.FadeOut(() =>
        {
            mainMenuCanvas.SetActive(true);
            gameObject.SetActive(false);
            FadeAnimManager.Instance.FadeIn();
        });
    }
}
