﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject mainMenuCanvas;

    private void OnEnable()
    {
        InputManager.Instance.CanControl = false;
        InputManager.Instance.CanPause = false;

        GameManager.Instance.GamePaused = true;

        Time.timeScale = 0f;
    }

    private void OnDisable()
    {
        InputManager.Instance.CanControl = true;
        InputManager.Instance.CanPause = true;

        GameManager.Instance.GamePaused = false;

        Time.timeScale = 1f;
    }

    public void BTN_Continue()
    {
        gameObject.SetActive(false);
    }

    public void BTN_Retry()
    {
        GameManager.Instance.BeginGame();
    }

    public void BTN_MainMenu()
    {
        FadeAnimManager.Instance.FadeOut(() =>
        {
            gameObject.SetActive(false);
            GameManager.Instance.EndGame(true);
        });
    }
}
