﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    #region Game Variables
    [Header("Game Variables")]
    public GameObject[] playableShips;
    [HideInInspector]
    public GameObject player;

    public int lifes = 3;
    public int Lifes
    {
        get
        {
            return lifes;
        }
        set
        {
            lifes = value;
            HUDManager.Instance.UpdateLifes(lifes);
        }
    }
    public float deathDelay = 2f;

    [HideInInspector]
    public bool perfectClear = true;

    [HideInInspector]
    public bool allAsteroidsCleared
    {
        get
        {
            return ObjectPoolManager.Instance.CurrentlyActiveObjects(PooledObject.Asteroid) <= 0;
        }
    }

    public List<GameObject> spawnedAsteroids;
    public List<GameObject> spawnedEnemies;

    public bool GamePaused = false;
    #endregion

    #region Screen Limits
    [Header("Screen Limits")]
    public Vector2 screenLimits = new Vector2 (8, 5);
    public float safeCenterLimit = 2f;
    #endregion

    [HideInInspector]
    public int selectedShip = 1;

    [HideInInspector]
    public int currentStage = 1;

    [Header("Customization")]
    public Camera cam;
    public Color[] bgColors;
    public float endStageDelay = 3f;

    [Header("References")]
    public GameObject gameHUD;
    public GameObject mainMenuHUD;
    public GameObject pauseHUD;
    public GameObject highscoresHUD;
    public Text gameClearText;
    public GameObject perfectText;

    public void Start()
    {
        cam.backgroundColor = bgColors[0];

        ///Loads past scores
        ScoringManager.Instance.LoadHighscores();
    }

    public void Update()
    {
        if(InputManager.Instance.PausePressed && InputManager.Instance.CanPause && !pauseHUD.activeSelf)
        {
            pauseHUD.SetActive(true);
            InputManager.Instance.ResetTouches();
        }
    }

    #region Game-flow Controlling methods

    public void StartGame()
    {
        currentStage = 0;
        lifes = 3;
        ScoringManager.Instance.thisGameScore = new Score();
        ScoringManager.Instance.thisGameScore.points = 0;

        StartStage();
    }

    private void StartStage()
    {
        ClearScreen();

        InputManager.Instance.CanPause = true;
        gameClearText.transform.parent.gameObject.SetActive(false);

        InputManager.Instance.ShowMobileInput(true);

        currentStage++;

        cam.backgroundColor = bgColors[currentStage % bgColors.Length];

        spawnedAsteroids = new List<GameObject>();
        spawnedEnemies = new List<GameObject>();

        for (int i = 0; i < currentStage; i++)
        {
            var ast = ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.Asteroid, RandomAsteroidPosition(), Quaternion.identity);
            var astInstance = ast.GetComponent<AsteroidInstance>();
            astInstance.thisAsteroid = new Asteroid(3);
            astInstance.Initialize();

            spawnedAsteroids.Add(ast);
        }

        if(currentStage > 1)
        {
            SpawnEnemies();
        }

        InputManager.Instance.CanControl = true;
        perfectClear = true;
        SpawnPlayer();

        FadeAnimManager.Instance.FadeIn();
    }

    public void BeginGame()
    {
        SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.GameStart);

        FadeAnimManager.Instance.FadeOut(() =>
        {
            Time.timeScale = 1f;
            StartGame();
            mainMenuHUD.SetActive(false);
            highscoresHUD.SetActive(false);
            pauseHUD.SetActive(false);
        });
    }
    #endregion

    private void SpawnPlayer(bool blinking = false)
    {
        player = Instantiate(playableShips[selectedShip], Vector2.zero, Quaternion.identity);
        var shipStats = player.GetComponent<ShipInstance>().ship.stats;

        if (blinking)
        {
            SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.Respawn);
            player.GetComponent<ShipInstance>().BlinkShip();
        }

        gameHUD.SetActive(true);
        HUDManager.Instance.ResetHUD(shipStats.shieldIcon, shipStats.lifeIcon, shipStats.HP, lifes);
    }

    private Vector2 RandomAsteroidPosition()
    {
        var vec = new Vector2();

        do
        {
            vec.x = UnityEngine.Random.Range(-screenLimits.x, screenLimits.x);
        }
        while (vec.x < safeCenterLimit && vec.x > -safeCenterLimit);

        do
        {
            vec.y = UnityEngine.Random.Range(-screenLimits.y, screenLimits.y);
        }
        while (vec.y < safeCenterLimit && vec.y > -safeCenterLimit);

        return vec;
    }

    private void SpawnEnemies()
    {
        int enemyCount = Mathf.FloorToInt(currentStage / 2);

        for(int i = 0; i < enemyCount; i++)
        {
            ///Randomizes between types of enemies
            float eneRand = UnityEngine.Random.value;

            var ene = ObjectPoolManager.Instance.SpawnPooledObject
                (eneRand < 0.5f ? PooledObject.BigUFOEnemy : PooledObject.SmallUFOEnemy, RandomAsteroidPosition(), Quaternion.identity);

            spawnedEnemies.Add(ene);
        }
    }

    void ClearScreen()
    {
        if (player != null) Destroy(player);
        ClearAsteroids();
        ClearEnemies();
    }

    void ClearAsteroids()
    {
        foreach(GameObject go in spawnedAsteroids)
        {
            go.SetActive(false);
        }
    }

    void ClearEnemies()
    {
        foreach (GameObject go in spawnedEnemies)
        {
            go.SetActive(false);
        }
    }

    #region Screen animation-related Methods
    public void AsteroidDestroyed()
    {
        if(allAsteroidsCleared)
        {
            EndStage();
        }
    }

    private void EndStage()
    {
        InputManager.Instance.CanControl = false;
        InputManager.Instance.CanPause = false;
        InputManager.Instance.ResetTouches();

        player.GetComponent<Collider2D>().enabled = false;

        gameHUD.SetActive(false);
        InputManager.Instance.ShowMobileInput(false);

        if(perfectClear)
        {
            SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.PerfectClear);
            ScoringManager.Instance.EarnPoints(ScoreType.PerfectClear);
        }
        else
        {
            SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.StageClear);
        }

        gameClearText.transform.parent.gameObject.SetActive(true);
        gameClearText.text = "STAGE " + currentStage.ToString() + " CLEAR!";
        perfectText.SetActive(perfectClear);

        StartCoroutine(EndStageDelay());
    }

    private IEnumerator EndStageDelay()
    {
        yield return new WaitForSecondsRealtime(endStageDelay);

        FadeAnimManager.Instance.FadeOut(() =>
        {
            Destroy(player);
            StartStage();
        });
    }

    public void LoseLife()
    {
        StartCoroutine(DeathDelay());
    }

    public IEnumerator DeathDelay()
    {
        yield return new WaitForSeconds(deathDelay);

        Lifes--;

        if(Lifes > 0)
        {
            SpawnPlayer(true);
        }
        else
        {
            gameHUD.SetActive(false);
            InputManager.Instance.ShowMobileInput(false);

            GameOver();
        }
    }

    private void GameOver()
    {
        SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.GameOver);

        InputManager.Instance.CanControl = false;
        InputManager.Instance.CanPause = false;

        gameHUD.SetActive(false);
        InputManager.Instance.ShowMobileInput(false);

        gameClearText.transform.parent.gameObject.SetActive(true);
        gameClearText.text = "GAME OVER";
        perfectText.SetActive(false);

        StartCoroutine(GameOverDelay());
    }

    private IEnumerator GameOverDelay()
    {
        yield return new WaitForSecondsRealtime(endStageDelay);

        FadeAnimManager.Instance.FadeOut(() =>
        {
            gameClearText.transform.parent.gameObject.SetActive(false);
            EndGame();
        });
    }

    public void EndGame(bool abort = false)
    {
        ClearScreen();
        gameHUD.SetActive(false);

        if (!abort)
        {
            ScoringManager.Instance.ApplyScoreSession();
            highscoresHUD.SetActive(true);
        }
        else
        {
            mainMenuHUD.SetActive(true);
        }

        FadeAnimManager.Instance.FadeIn();
    }
    #endregion
}