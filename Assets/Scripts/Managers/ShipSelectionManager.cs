﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipSelectionManager : MonoBehaviour
{
    [Header("Prefabs")]
    public ShipMenuButton buttonPrefab;

    [Header("References")]
    public GridLayoutGroup selectionButtonGrids;
    public GameObject mainMenuHUD;
    public GameObject highscoresHUD;
    public Text shipName;
    public Image shipBGImage;
    public MenuShipStat[] menuStats;

    private List<ShipMenuButton> selectableShips;

    public void OnEnable()
    {
        GameManager.Instance.currentStage = 0;
    }

    public void Start()
    {
        selectableShips = new List<ShipMenuButton>();

        for (int i = 0; i < GameManager.Instance.playableShips.Length; i++)
        {
            ShipMenuButton shipBtn = Instantiate(buttonPrefab, selectionButtonGrids.transform);

            ///Avoids lambda errors.
            var id = i;

            shipBtn.ShipID = id;
            shipBtn.shipPrefab = GameManager.Instance.playableShips[id];
            shipBtn.shipIconSprite = shipBtn.shipPrefab.GetComponent<SpriteRenderer>().sprite;
            shipBtn.stats = GameManager.Instance.playableShips[id].GetComponent<ShipInstance>().ship.stats;
            shipBtn.button.onClick.AddListener(() => OnShipSelected(id));

            selectableShips.Add(shipBtn);
        }

        ///Selects the first ship by default;
        OnShipSelected(0);
    }

    public void OnShipSelected(int ship)
    {
        GameManager.Instance.selectedShip = ship;
        shipName.text = selectableShips[ship].stats.ShipName;
        shipBGImage.sprite = selectableShips[ship].shipIconSprite;
        UpdateShipStats(selectableShips[ship].stats);
    }

    public void UpdateShipStats(ShipStats stats)
    {
        menuStats[0].SetStars(Mathf.CeilToInt((Mathf.InverseLerp(1, 4, stats.HP) * 5)));
        menuStats[1].SetStars(Mathf.RoundToInt((Mathf.InverseLerp(1.5f, 5, stats.Steering) * 5)));
        menuStats[2].SetStars(Mathf.RoundToInt((Mathf.InverseLerp(0.008f, 0.15f, stats.Thrust) * 5)));
        menuStats[3].SetStars(Mathf.RoundToInt((Mathf.InverseLerp(0.035f, 0.12f, stats.MaxSpeed) * 5)));
        menuStats[4].SetStars(Mathf.RoundToInt(((1 - Mathf.InverseLerp(0.98f, 1, stats.DeaccelRate)) * 5)));
        menuStats[5].SetStars(Mathf.RoundToInt(((1 - Mathf.InverseLerp(0.2f, 0.6f, stats.FireCooldown)) * 5)));
    }

    public void BTN_ShowHighscores()
    {
        FadeAnimManager.Instance.FadeOut(() =>
        {
            mainMenuHUD.SetActive(false);
            highscoresHUD.SetActive(true);
            FadeAnimManager.Instance.FadeIn();
        });
    }
}
