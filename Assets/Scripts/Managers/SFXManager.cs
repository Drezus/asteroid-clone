﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : Singleton<SFXManager>
{
    public enum SoundEffects
    {
        GameStart,
        GameOver,
        StageClear,
        PerfectClear,
        Shoot,
        Hit,
        Destroy,
        Respawn
    }

    public AudioSource source;

    [Header("Sound Effect References")]
    public AudioClip[] sounds;

    public void PlaySFX(SoundEffects sfx)
    {
        source.PlayOneShot(sounds[(int)sfx]);
    }
}
