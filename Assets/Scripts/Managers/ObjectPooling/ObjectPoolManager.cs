﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PooledObject
{
    Asteroid,
    Laser,
    LaserHitFX,
    AsteroidDebris,
    ShipDebris,
    HitEffect,
    ShieldIconHUD,
    EnemyLaser,
    EnemyLaserHitFX,
    BigUFOEnemy,
    SmallUFOEnemy
}

public class ObjectPoolManager : Singleton<ObjectPoolManager>
{
    [Header("Managed Pools")]
    public ObjectPool[] Pools;

    public void Awake()
    {
        foreach (ObjectPool pool in Pools)
        {
            InitializePool(pool);
        }
    }

    private void InitializePool(ObjectPool objPool)
    {
        objPool.pool = new Queue<GameObject>();

        for (int i = 0; i < objPool.poolSize; i++)
        {
            GameObject obj = Instantiate(objPool.prefabs[Random.Range(0, objPool.prefabs.Length)], objPool.poolParent);
            obj.SetActive(false);
            objPool.pool.Enqueue(obj);
        }
    }

    public GameObject SpawnPooledObject(PooledObject obj, Vector2 pos, Quaternion rot)
    {
        GameObject gameObj = Pools[(int)obj].pool.Dequeue();

        gameObj.transform.position = pos;
        gameObj.transform.rotation = rot;
        gameObj.SetActive(true);

        Pools[(int)obj].pool.Enqueue(gameObj);

        return gameObj;
    }

    public int CurrentlyActiveObjects(PooledObject obj)
    {
        /// -1 excludes the Parent transform from the count.
        return Pools[(int)obj].poolParent.GetComponentsInChildren<Transform>().Length - 1;
    }
}
