﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class ObjectPool
{
    public string Name;

    ///Pools may have different styles of prefabs.
    public GameObject[] prefabs;
    public int poolSize;
    public Transform poolParent;

    [HideInInspector]
    public Queue<GameObject> pool;
}
