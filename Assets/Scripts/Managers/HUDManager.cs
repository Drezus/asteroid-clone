﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : Singleton<HUDManager>
{
    [Header("References")]
    public Text scoreText;
    public Image lifeIcon;
    public Text lifeText;
    public Text stageText;

    [HideInInspector]
    Sprite shieldIconSprite;

    private Queue<GameObject> shieldIcons;

    public void ResetHUD(Sprite _shi, Sprite _lif, int _hp, int _lifes)
    {
        lifeIcon.sprite = _lif;
        shieldIconSprite = _shi;

        if (shieldIcons != null)
        {
            foreach (GameObject GO in shieldIcons)
            {
                GO.SetActive(false);
            }
        }

        shieldIcons = new Queue<GameObject>();

        lifeText.text = _lifes.ToString();

        UpdateShieldIcons(_hp);
        UpdateScoreText(ScoringManager.Instance.thisGameScore.points);

        stageText.text = "STAGE " + GameManager.Instance.currentStage.ToString();
    }

    public void UpdateScoreText(int score)
    {
        scoreText.text = score.ToString();
    }

    public void UpdateLifes(int li)
    {
        lifeIcon.GetComponent<Animator>().SetTrigger("Blink");
        lifeText.text = li.ToString();
    }

    public void UpdateShieldIcons(int _hp)
    {
        int currentIcons = shieldIcons.Count;

        if (_hp > currentIcons)
        {
            for (int i = 0; i < _hp - currentIcons; i++)
            {
                var ico = ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.ShieldIconHUD, Vector2.zero, Quaternion.identity);
                ico.GetComponent<Image>().sprite = shieldIconSprite;
                shieldIcons.Enqueue(ico);
            }
        }
        else if (_hp < currentIcons)
        {
            for (int i = 0; i < currentIcons - _hp; i++)
            {
                var icoToRemove = shieldIcons.Dequeue();
                icoToRemove.gameObject.SetActive(false);
            }
        }
    }

}
