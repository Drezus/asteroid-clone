﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Transform))]
public class WrappableTransform : MonoBehaviour
{
    void Update()
    {
        WrapHorizontally();
        WrapVertically();
    }

    private void WrapHorizontally()
    {
        var widthOffset = transform.localScale.x / 2;

        if (transform.position.x > GameManager.Instance.screenLimits.x + widthOffset)
        {
            transform.position = new Vector3(-GameManager.Instance.screenLimits.x - widthOffset, transform.position.y);
        }
        else if (transform.position.x < -GameManager.Instance.screenLimits.x - widthOffset)
        {
            transform.position = new Vector3(GameManager.Instance.screenLimits.x + widthOffset, transform.position.y);
        }
    }

    private void WrapVertically()
    {
        var heightOffset = transform.localScale.y / 2;

        if (transform.position.y > GameManager.Instance.screenLimits.y + heightOffset)
        {
            transform.position = new Vector3(transform.position.x, -GameManager.Instance.screenLimits.y - heightOffset);
        }
        else if (transform.position.y < -GameManager.Instance.screenLimits.y - heightOffset)
        {
            transform.position = new Vector3(transform.position.x, GameManager.Instance.screenLimits.y + heightOffset);
        }
    }
}
