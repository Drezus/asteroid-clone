﻿using System.Collections;
using UnityEngine;

public class ShipInstance : MonoBehaviour
{
    [Header("Components")]
    public bool AIControl = false;

    [SerializeField]
    public Ship ship;
    public GameObject fireParent;
    public Transform[] nozzles;
    public Collider2D coll;
    Renderer[] allRenderers;

    Vector3 acceleration = Vector2.zero;
    [HideInInspector] public bool boostVisible = false;

    Coroutine invulnerabilityCoroutine;
    float invulnerabilityTimer = 0f;
    public float invulnerabilityDuration = 2f;

    Coroutine blinkCoroutine;
    public float blinkDuration = 0.1f;


    public void OnEnable()
    {
        invulnerabilityTimer = 0f;
        ship.OnDestroy += OnShipDestroyed;

        ship.maxHP = ship.stats.HP;
        ship.currentHP = ship.stats.HP;

        allRenderers = GetComponentsInChildren<Renderer>();
    }

    public void OnDisable()
    {
        ship.OnDestroy -= OnShipDestroyed;
    }

    public void Update()
    {
        ///Updates the ship position according to the acceleration vector.
        if(!GameManager.Instance.GamePaused)
            transform.position = transform.position + acceleration;
    }

    public void LoseMomentum()
    {
        if(!GameManager.Instance.GamePaused)
        {
            if (acceleration.magnitude > 0)
            {
                acceleration = acceleration * ship.stats.DeaccelRate;
            }
        }
    }

    #region Input-related Methods
    public void Accelerate()
    {
        if (!boostVisible) ShowBoost(true);

        if ((acceleration + transform.up * ship.stats.Thrust * Time.deltaTime).magnitude < ship.stats.MaxSpeed)
        {
            acceleration += transform.up * ship.stats.Thrust * Time.deltaTime;
        }
    }

    public void Rotate(bool left)
    {
        if (left)
        {
            transform.Rotate(Vector3.forward * ship.stats.Steering);
        }
        else
        {
            transform.Rotate(Vector3.forward * -ship.stats.Steering);
        }
    }
    #endregion

    #region Animation-related Methods
    public void ShowBoost(bool show)
    {
        if (fireParent != null) fireParent.SetActive(show);
        boostVisible = show;
    }

    void OnShipDestroyed()
    {
        ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.ShipDebris, transform.position, Quaternion.identity);
        if (!AIControl)
        {
            GameManager.Instance.LoseLife();
            Destroy(gameObject);
        }
        else
        {
            SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.Destroy);

            if(transform.localScale.x > 1)
            {
                ScoringManager.Instance.EarnPoints(ScoreType.DestroyBigUFO);
            }
            else
            {
                ScoringManager.Instance.EarnPoints(ScoreType.DestroySmallUFO);
            }

            gameObject.SetActive(false);
        }
    }
    #endregion

    private void OnCollisionEnter2D(Collision2D coll)
    {
        string tag = coll.collider.tag;

        switch (tag)
        {
            case "Asteroid":
                if (gameObject.activeSelf)
                    ShipHit();
                break;
            case "Enemy":
                if (gameObject.activeSelf)
                    ShipHit();
                if (coll.gameObject.activeSelf)
                    coll.collider.GetComponent<ShipInstance>().ShipHit();
                break;
        }
    }

    public void ShipHit()
    {
        ship.TakeDamage(1);

        if (!AIControl)
        {
            BlinkShip(true);
        }
        else
        {
            if (ship.currentHP > 0) BlinkShip(true);
        }
 
        SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.Hit);
        if(!AIControl) GameManager.Instance.perfectClear = false;
    }

    public void BlinkShip(bool showHitParticle = false)
    {
        invulnerabilityCoroutine = StartCoroutine(InvulnerabilityBlink(showHitParticle));
    }

    public IEnumerator InvulnerabilityBlink(bool showHitParticle = false)
    {
        coll.enabled = false;

        if(!AIControl) HUDManager.Instance.UpdateShieldIcons(ship.currentHP);

        if(showHitParticle)
            ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.HitEffect, transform.position, Quaternion.identity);

        foreach (Renderer rend in allRenderers)
        {
            rend.enabled = false;
        }

        if (ship.currentHP > 0)
        {
            ///Blinks the sprite to indicate damage-invulnerability.
            blinkCoroutine = StartCoroutine(Blink());

            while (invulnerabilityTimer < invulnerabilityDuration)
            {
                invulnerabilityTimer += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            foreach (Renderer rend in allRenderers)
            {
                rend.enabled = true;
            }

            coll.enabled = true;

            StopCoroutine(blinkCoroutine);
            invulnerabilityTimer = 0f;
        }
    }

    private IEnumerator Blink()
    {
        while(true)
        {
            foreach (Renderer rend in allRenderers)
            {
                rend.enabled = false;
            }
            yield return new WaitForSeconds(blinkDuration);

            foreach (Renderer rend in allRenderers)
            {
                rend.enabled = true;
            }
            yield return new WaitForSeconds(blinkDuration);
        }
    }
}
