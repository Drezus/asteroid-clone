﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidInstance : MonoBehaviour
{
    [HideInInspector]
    public Asteroid thisAsteroid = null;

    Vector3 acceleration = Vector2.zero;

    public void Initialize()
    {
        if (thisAsteroid == null)
        {
            thisAsteroid = new Asteroid(3);
        }

        SetAcceleration(thisAsteroid.velocity /  Mathf.Clamp(thisAsteroid.maxHP, 1, thisAsteroid.maxHP));
        thisAsteroid.OnDestroy += OnAsteroidDestroyed;

        gameObject.transform.localScale = new Vector2(thisAsteroid.maxHP, thisAsteroid.maxHP);

        thisAsteroid.currentHP = thisAsteroid.maxHP;
    }

    public void Update()
    {
        ///Updates the asteroid position and rotation according to the acceleration vector
        ///and the torque float.
        if (!GameManager.Instance.GamePaused)
        {
            transform.position = transform.position + acceleration;
            Rotate();
        }
    }

    #region Movement-related Methods
    public void SetAcceleration(Vector2 vel)
    {
        acceleration = vel * Time.deltaTime;
    }

    public void Rotate()
    {
        transform.Rotate(Vector3.forward * thisAsteroid.torque);
    }
    #endregion

    #region Animation-related Methods
    public void OnHit()
    {
        thisAsteroid.TakeDamage(1);
    }

    void OnAsteroidDestroyed()
    {
        ///Either spawn new, smaller asteroids or just disables the tiny asteroid entirely
        if(thisAsteroid.maxHP > 1)
        {
            for (int i = 0; i < thisAsteroid.maxHP; i++)
            {
                AsteroidInstance smallerAst = ObjectPoolManager.Instance.SpawnPooledObject
                    (PooledObject.Asteroid, transform.position, Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0f, 360f))).GetComponent<AsteroidInstance>();
                smallerAst.thisAsteroid = new Asteroid(thisAsteroid.maxHP - 1);
                smallerAst.Initialize();

                GameManager.Instance.spawnedAsteroids.Add(smallerAst.gameObject);
            }
        }

        ///Bigger asteroids are worth less points.
        ScoringManager.Instance.EarnPoints(ScoringManager.Instance.scoreValues[ScoreType.DestroyAsteroid] / thisAsteroid.maxHP);

        ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.AsteroidDebris, transform.position, Quaternion.identity);

        SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.Destroy);

        gameObject.SetActive(false);
        GameManager.Instance.AsteroidDestroyed();
    }
    #endregion
}

