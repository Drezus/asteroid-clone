﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuShipStat : MonoBehaviour
{
    public GameObject[] starSprites;

    public void SetStars(int _stars)
    {
        _stars = Mathf.Clamp(_stars, 1, 5);

        for (int i = 0; i < 5; i++)
        {
            starSprites[i].SetActive(false);
        }

        for (int i = 0; i < _stars; i++)
        {
            starSprites[i].SetActive(true);
        }
    }
}
