﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAnimManager : Singleton<FadeAnimManager>
{
    public Animator anim;
    public delegate void FadeCallback();
    public float fadeDelay = 1f;
    private bool fading = false;

    private void Start()
    {
        FadeIn();
    }

    public void FadeOut(FadeCallback callback = null)
    {
        if(!fading)
            StartCoroutine(FadeOutCoroutine(callback));
    }

    public void FadeIn(FadeCallback callback = null)
    {
        if (!fading)
            StartCoroutine(FadeInCoroutine(callback));
    }

    private IEnumerator FadeOutCoroutine(FadeCallback callback = null)
    {
        fading = true;
        anim.SetTrigger("FadeOut");

        yield return new WaitForSecondsRealtime(fadeDelay);

        fading = false;
        if (callback != null) callback();

    }

    private IEnumerator FadeInCoroutine(FadeCallback callback = null)
    {
        fading = true;
        anim.SetTrigger("FadeIn");

        yield return new WaitForSecondsRealtime(fadeDelay);

        fading = false;
        if (callback != null) callback();
    }
}
