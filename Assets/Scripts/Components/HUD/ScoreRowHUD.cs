﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreRowHUD : MonoBehaviour
{
    [Header("References")]
    public Text pointsText;
    public Text stageText;
    public Text shipNameText;
    public Image shipImage;

    public void SetRow(Score score)
    {
        pointsText.text = score.points.ToString();
        stageText.text = score.stage.ToString();

        var shipStats = GameManager.Instance.playableShips[score.ship].GetComponent<ShipInstance>();

        shipNameText.text = shipStats.ship.stats.ShipName;
        shipImage.sprite = shipStats.GetComponent<SpriteRenderer>().sprite;
    }
}
