﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipMenuButton : MonoBehaviour
{
    #region References
    public Image shipIcon;
    public Button button;
    #endregion

    #region Variables
    public Sprite shipIconSprite;
    private int shipID;

    [HideInInspector]
    public int ShipID
    {
        get
        {
            return shipID;
        }
        set
        {
            shipID = value;
            shipIcon.sprite = GameManager.Instance.playableShips[value].GetComponent<SpriteRenderer>().sprite;

            button.onClick.RemoveAllListeners();
        }
    }
    [HideInInspector]
    public GameObject shipPrefab;
    [HideInInspector]
    public ShipStats stats;
    #endregion
}
