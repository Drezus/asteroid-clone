﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisParticle : MonoBehaviour
{
    public ParticleSystem particles;

    /// <summary>
    /// Listens to the Particle System state and self-recycles once it has ended.
    /// </summary>
    private void Update()
    {
        if (particles.isStopped)
        {
            SelfRecycle();
        }
    }

    /// <summary>
    /// The Object Pool Manager already handles reutilizing objects,
    /// so we can just simple turn it off for now.
    /// </summary>
	void SelfRecycle()
    {
        gameObject.SetActive(false);
    }
}
