﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [Header("Configs")]
    public float laserSpeed = 6f;
    public float laserLifetime = 1.5f;
    public bool enemyLaser = false;

    Coroutine lifetimeCorroutine;
    float lifeCounter;

    void OnEnable()
    {
        lifetimeCorroutine = StartCoroutine(LaserLifetime());
    }

    IEnumerator LaserLifetime()
    {
        lifeCounter = 0f;

        while (lifeCounter < laserLifetime)
        {
            lifeCounter += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        DiscardLaser();
    }

    void Update ()
    {
        ///Updates the laser position
        transform.Translate(transform.up * laserSpeed * Time.deltaTime, Space.World);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        string tag = coll.collider.tag;

        switch (tag)
        {
            case "Player":
                if (enemyLaser)
                {
                    coll.collider.GetComponent<ShipInstance>().ShipHit();
                    LasetHit();
                }
                break;
            case "Enemy":
                if (!enemyLaser)
                {
                    coll.collider.GetComponent<ShipInstance>().ShipHit();
                    LasetHit();
                }
                break;
            case "Asteroid":
                coll.collider.GetComponent<AsteroidInstance>().OnHit();
                LasetHit();
                break;
            case "Laser":
                LasetHit();
                break;
        }
    }

    ///Since Object Pool Manager already handles recycling spawned objects, we just need to reset variables and turn this one off for now.
    void LasetHit()
    {
        ///Spawns the hit effect
        if(!enemyLaser)
        {
            ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.LaserHitFX, transform.position, Quaternion.identity);
        }
        else
        {
            ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.EnemyLaserHitFX, transform.position, Quaternion.identity);
        }
        DiscardLaser();
    }

    void DiscardLaser()
    {
        StopCoroutine(lifetimeCorroutine);
        gameObject.SetActive(false);
    }
}
