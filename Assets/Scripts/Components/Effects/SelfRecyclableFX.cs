﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfRecyclableFX : MonoBehaviour
{
    /// <summary>
    /// The Object Pool Manager already handles reutilizing objects,
    /// so we can just simple turn it off for now.
    /// </summary>
	void SelfRecycle()
    {
        gameObject.SetActive(false);
    }
}
