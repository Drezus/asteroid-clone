﻿using System;
using UnityEngine;

[Serializable]
public class Score
{
    public int points = 0;
    public int stage = 0;
    public int ship = 0;
}