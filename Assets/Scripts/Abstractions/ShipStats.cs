﻿using System;
using UnityEngine;

[Serializable]
public class ShipStats : ScriptableObject
{
    public string ShipName;
    public int HP = 10;
    public float Steering = 5f;
    public float Thrust = 0.1f;
    public float MaxSpeed = 10f;
    public float DeaccelRate = 0.99f;
    public float FireCooldown = 0.5f;
    public Sprite lifeIcon;
    public Sprite shieldIcon;
}