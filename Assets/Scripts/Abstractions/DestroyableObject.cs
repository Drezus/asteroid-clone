﻿using System;
using UnityEngine;

public abstract class DestroyableObject
{
    [HideInInspector]
    public int maxHP;

    [HideInInspector]
    public int currentHP;

    public delegate void DestroyCallback();
    public DestroyCallback OnDestroy;

    public void TakeDamage(int damage)
    { 
        currentHP -= damage;

        if(currentHP - damage < 0)
        {
            currentHP = 0;
            if (OnDestroy != null)
                OnDestroy();
        }
    }
}