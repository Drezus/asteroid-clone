﻿using System;
using UnityEngine;

[Serializable]
public class Ship : DestroyableObject
{
    [SerializeField]
    public ShipStats stats;

    [HideInInspector]
    public Ship(string _name, float _steer, float _thrust, float _maxSpd)
    {
        maxHP = stats.HP;

        stats.ShipName = _name;
        currentHP = maxHP;
        stats.Steering = _steer;
        stats.Thrust = _thrust;
        stats.MaxSpeed = _maxSpd;
    }
}
