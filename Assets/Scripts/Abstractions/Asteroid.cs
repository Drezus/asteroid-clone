﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : DestroyableObject
{
    #region Constants
    const float minVelocity = -2.5f;
    const float maxVelocity = 2.5f;
    const float minTorque = -2f;
    const float maxTorque = 2f;
    #endregion

    public Vector2 velocity;
    public float torque;

    public Asteroid(int _hp)
    {
        maxHP = _hp;
        currentHP = _hp;

        velocity = new Vector2(UnityEngine.Random.Range(minVelocity, maxVelocity), UnityEngine.Random.Range(minVelocity, maxVelocity));
        torque = UnityEngine.Random.Range(minTorque, maxTorque);
    }
}
