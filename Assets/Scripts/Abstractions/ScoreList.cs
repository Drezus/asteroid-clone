﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ScoreList
{
    public List<Score> scoreList;
}