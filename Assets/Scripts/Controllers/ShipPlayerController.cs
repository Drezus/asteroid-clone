﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShipInstance))]
public class ShipPlayerController : MonoBehaviour
{
    ShipInstance shipToControl;

    bool canShoot = true;
    int currentNozzle = 0;

	void OnEnable()
    {
        shipToControl = GetComponent<ShipInstance>();
        canShoot = true;
        currentNozzle = 0;
    }

    /// <summary>
    /// Input listening
    /// </summary>
    void Update()
    {
        if (InputManager.Instance.LeftPressed)
        {
            shipToControl.Rotate(true);
        }

        if (InputManager.Instance.RightPressed)
        {
            shipToControl.Rotate(false);
        }

        if (InputManager.Instance.ThrustPressed)
        {
            shipToControl.Accelerate();
        }
        else
        {
            if(shipToControl.boostVisible) shipToControl.ShowBoost(false);
            shipToControl.LoseMomentum();
        }

        if(InputManager.Instance.FirePressed && canShoot)
        {
            StartCoroutine(FireAndCooldown());
        }
    }

    IEnumerator FireAndCooldown()
    {
        canShoot = false;

        ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.Laser, 
            shipToControl.nozzles[currentNozzle].transform.position, shipToControl.nozzles[currentNozzle].transform.rotation);

        ///Cycles between nozzles
        currentNozzle = currentNozzle + 1 > shipToControl.nozzles.Length - 1 ? 0 : currentNozzle + 1;

        SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.Shoot);

        yield return new WaitForSeconds(shipToControl.ship.stats.FireCooldown);
        canShoot = true;
    }
}
