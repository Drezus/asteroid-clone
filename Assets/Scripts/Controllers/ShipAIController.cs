﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShootingMode
{
    Random,
    TargetsPlayer
}

[RequireComponent(typeof(ShipInstance))]
public class ShipAIController : MonoBehaviour
{
    ShipInstance shipToControl;

    bool canShoot = true;
    int currentNozzle = 0;

    [Header ("IA Variables")]
    public float moveYield = 2f;
    private float moveTimer = 0f;

    public delegate void AIAction();
    public AIAction action;

    private Coroutine AIActionCoroutine;

    public ShootingMode shootMode;

    [HideInInspector]
    public Transform playerPos;

	void OnEnable()
    {
        shipToControl = GetComponent<ShipInstance>();
        canShoot = true;
        currentNozzle = 0;

        SelectNextAction();
    }

    #region IA-Related Methods
    private void SelectNextAction()
    {
        if(AIActionCoroutine != null ) StopCoroutine(AIActionCoroutine);

        ///Randomly selects between 4 distinct states
        ///to perform for the next seconds.
        int nextAction = UnityEngine.Random.Range(1, 4);

        switch (nextAction)
        {
            case 1:
                action = () => shipToControl.Rotate(true);
                break;
            case 2:
                action = () => shipToControl.Rotate(false);
                break;
            case 3:
                action = () => shipToControl.Accelerate();
                break;
            case 4:
            default:
                action = () => shipToControl.LoseMomentum();
                break;
        }

        AIActionCoroutine = StartCoroutine(IAMove());
    }

    private IEnumerator IAMove()
    {
        if (!GameManager.Instance.GamePaused)
        {
            while(moveTimer < moveYield)
            {
                moveTimer += Time.deltaTime;
                action();
                yield return new WaitForEndOfFrame();
            }

            moveTimer = 0f;
            SelectNextAction();
            yield return null;
        }
    }
    #endregion

    /// <summary>
    /// Shoots randomly now-stop
    /// </summary>
    public void Update()
    {
        if (!GameManager.Instance.GamePaused)
        {
            if(canShoot)
            {
                StartCoroutine(FireAndCooldown());
            }
        }
    }
  
    IEnumerator FireAndCooldown()
    {
        canShoot = false;

        if (shootMode == ShootingMode.Random)
        {
            ///Cycles randomly between nozzles
            currentNozzle = UnityEngine.Random.Range(0, shipToControl.nozzles.Length);

            ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.EnemyLaser,
                shipToControl.nozzles[currentNozzle].transform.position, shipToControl.nozzles[currentNozzle].transform.rotation);

            SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.Shoot);
        }
        else
        {
            playerPos = null;
            try
            {
                ///Aims for the player
                playerPos = GameObject.FindWithTag("Player").transform;
            }
            catch
            {
                playerPos = null;
            }

            ///Only shoots if there's a player
            if(playerPos != null)
            {
                var laser = ObjectPoolManager.Instance.SpawnPooledObject(PooledObject.EnemyLaser,
                    shipToControl.transform.position, Quaternion.identity);

                Vector3 dir = playerPos.position - transform.position;
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                laser.transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

                SFXManager.Instance.PlaySFX(SFXManager.SoundEffects.Shoot);
            }
        }

        yield return new WaitForSeconds(shipToControl.ship.stats.FireCooldown);
        canShoot = true;
    }
}
