﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NewShipEditor
{
    [MenuItem("Assets/Create/New Asteroids Ship Stats")]
    public static void CreateNewShip()
    {
        ShipStats asset = ScriptableObject.CreateInstance<ShipStats>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Ship Stats/NewShip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
